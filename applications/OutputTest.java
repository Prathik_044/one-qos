import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;

public class OutputTest {
	public static void main (String args[]) {
		Writer writer =null;
			
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream("../reports/RW3-DMsgTest/DeliveryGraph.txt",true),"utf-8"));
			writer.write("\nMSG_ID TIME NODEID\n");
			writer.write("Something,Something,Something\n");
		} catch (IOException ex) {
			System.out.println("Error on writing to file\n");
		}finally {
			try{writer.close();}catch (Exception ex){}
			}
	}
}