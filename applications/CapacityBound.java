package applications;

import core.Application;
import core.DTNHost;
import core.Message;
import core.SimClock;
import core.SimScenario;
import core.Settings;
import core.World;

import java.util.List;
/**
 * The use of this application is to help determine a capacity bound for message transger in 
 * intermittently connected networks as proposed in the paper "Guaranteed Capacity Bounds in 
 * Intermittently-Connected Networks: a Resource-Aware, Holistic Evaluation" by Gabriel Sandulescu
 * et al.
 * @author Keystroke
 *
 */
public class CapacityBound extends Application {

	/** Application ID */
	public static final String APP_ID = "CapacityBound_Guarantee";
	public static final int max_kmsgs = 50;
	public int num_msgs = 0;
	int seed;
	Settings s = new Settings("MovementModel");
	
	public CapacityBound(Settings S) {
		super.setAppID(APP_ID);
	}
	
	public CapacityBound(CapacityBound a) {
		super(a);
	}
/**
 * This function is where the modification of the d-messages if the current node is not the destination and
 * storing of information to build the delivery graph if the current node is the destination is to be done.
 */
	@Override
	public Message handle(Message msg, DTNHost host) {
		/*
		 *  Check if the current node is the destination node
		 *  If not add the extra d message info to the parameters
		 *  in the d message using the hop count.
		 */
		
		if ((msg.getTo()!=host) && (msg.getTo().getAddress() == 1)) {
			
			List<DTNHost> list = msg.getHops();
			if(msg.getProperty("MsgDel") == null) {
				msg.addProperty("MsgDel",""+list.get(list.size()-1)+","+host.getAddress()+","+
										host.getFreeBufferSize()+","+SimClock.getTime()+"\n");
			}
			
			else {
				msg.updateProperty("MsgDel", msg.getProperty("MsgDel")+""+list.get(list.size()-1)+","+host.getAddress()+","+
						host.getFreeBufferSize()+","+SimClock.getTime()+"\n");
			}
			
			return msg;
		}
		
		/*
		 * The logic currently followed here is to build the delivery graph 
		 * out of the first messages reaching the destination without worrying
		 * about duplicates etc. Since computation has been shifted off simulation
		 * no other main logic is to be implemented at end node apart from
		 * the above and another relating to storing/writing the message
		 * properties to file for post processing.
		 */
		else if((msg.getTo() == host) && (host.getAddress() == 1)) {
			/*
			 * To use the functions simply, the seed of the current iteration
			 * is sent as the event, which is parsed as int in the gotEvent function
			 */
			if((msg.getProperty("type").equals("k_msg"))) {
				msg.updateProperty("MsgDel", msg.getProperty("MsgDel")+""+(msg.getReceiveTime()-msg.getCreationTime()));
				super.sendEventToListeners("KMsg_Del", msg, host);
			}
			return msg;
		}
		/*
		 * The else case deals with nodes that are the destination but not the one
		 * under consideration.
		 */
		else {
			return msg;
		}
	}

		/*
		 * Functionality for triggering k-message transmission from host
		 */
	@Override
	public void update(DTNHost host) {
		World w = SimScenario.getInstance().getWorld();
		//if(SimClock.getIntTime() >= 150000) {
			if(host.getAddress() == 0) {
				if(num_msgs < max_kmsgs) {
					num_msgs ++;
					Message m = new Message(host,w.getNodeByAddress(1),"K_Msg:"+num_msgs+"_"+SimClock.getIntTime(),1);
					m.addProperty("type","k_msg");
					m.setAppID(APP_ID);
					host.createNewMessage(m);
				}
			}
		//}
	}

	@Override
	public Application replicate() {
		// TODO Auto-generated method stub
		return new CapacityBound(this);
	}

}
