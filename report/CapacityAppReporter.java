package report;

import core.Application;
import core.ApplicationListener;
import core.DTNHost;
import core.Message;

import java.util.Set;

public class CapacityAppReporter extends Report implements ApplicationListener {


	public CapacityAppReporter() {
		init();
	}
	
	@Override
	public void init() {
		super.init();
	}
	
	@Override
	public void gotEvent(String event, Object params, Application app, DTNHost host) {
		Message m = (Message) params;
		//Typecasting the message and calling the function
		writeMsg(m,app,host);
	}
	
	public void writeMsg(Message msg, Application app, DTNHost host) {
				
		//write(msg.getId() + " " + msg.getHopCount());
		//write("Address,FreeBufferSize,Time");
		Set<String> keys = msg.getPropertyKeys();
		for (String key : keys) {
			if(!key.equals("type")) {
				write(msg.getProperty(key).toString());
			}
		}

	}

	
	@Override
	public void done() {
		super.done();
			}
}
